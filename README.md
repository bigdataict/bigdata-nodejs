# Instructies
Installeer [NodeJS](https://nodejs.org/en/)

Stel de Twitter API keys in in 
`storeTwitterStream.js`

Gebruik een command shell naar keuze bijv. Git Bash of gewoon CMD en executeer de volgende command met als root de map waar je het script hebt:

`node storeTwitterStream.js`